#
# Be sure to run `pod lib lint CommonSwiftSDKKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CommonSwiftSDKKit'
  s.version          = '0.0.1'
  s.summary          = 'A short description of CommonSwiftSDKKit.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/gwave_mobile_public/CommonSwiftSDKKit'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'yangpeng' => 'peng.yang@kikitrade.com' }
  s.source           = { :git => 'git@gitlab.com:gwave_mobile_public/CommonSwiftSDKKit.git', :tag => s.version.to_s }

  s.ios.deployment_target = '14.0'
  s.static_framework = true
#  s.source_files = 'CommonSwiftSDKKit/**/*'
  s.default_subspec = 'Source'
  s.subspec 'Source' do |spec|
      spec.source_files = 'CommonSwiftSDKKit/Source/**/*'
  end
  
  s.dependency 'FingerprintJS', '1.3.0'
  s.dependency 'Web3AuthMugen', '5.0.0'
  s.dependency 'FirebaseDynamicLinks', '10.10.0'
  s.dependency 'SwiftyCrypto', '0.0.1'

  s.swift_version = '5.0'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
