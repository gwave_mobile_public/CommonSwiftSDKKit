//
//  FingerprintJSBridge.swift
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2023/5/6.
//

import Foundation
import FingerprintJS

@objc public class FingerprintJSbridge: NSObject {
    
    private let fingerprinter = FingerprinterFactory.getInstance()
    
    @objc public func generateTree() async -> String {
        return await fingerprinter.getFingerprintTree().fingerprint
    }
    
    @objc public func generateDevicedId() async -> String {
        return await fingerprinter.getDeviceId() ?? ""
    }
}
