//
//  FireBaseBridge.h
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2023/6/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^CompletionHandler)(NSString *link);
typedef void(^ShareResult)(BOOL isSuccess);

@interface FireBaseBridge : NSObject

- (void)configure;

- (BOOL)handleUniversalLink:(NSURL *)universalLinkURL completion:(CompletionHandler)completion;

@end

NS_ASSUME_NONNULL_END
