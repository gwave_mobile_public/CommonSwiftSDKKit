//
//  FireBaseBridge.m
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2023/6/13.
//

#import "FireBaseBridge.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseDynamicLinks/FirebaseDynamicLinks.h>

@implementation FireBaseBridge

- (void)configure
{
    [FIRApp configure];
}

- (BOOL)handleUniversalLink:(NSURL *)universalLinkURL completion:(CompletionHandler)completion;
{
    BOOL handled = [[FIRDynamicLinks dynamicLinks] handleUniversalLink:universalLinkURL completion:^(FIRDynamicLink * _Nullable dynamicLink, NSError * _Nullable error) {
        NSLog(@"dynamiclink=%@",dynamicLink.url);
        completion(dynamicLink.url.absoluteString);
    }];
    return handled;
}

@end
