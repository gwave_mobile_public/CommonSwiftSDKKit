//
//  JwtBridge.swift
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2023/7/11.
//

import Foundation
import SwiftyCrypto
import CryptoKit

@objc public class JwtBridge: NSObject {
    @objc public func createJwt(secretKey:String, payload:[String: Any]) -> String {
        let header: [String: Any] = [
            "alg": "HS256",
            "typ": "JWT"
        ]
        let payload: [String: Any] = payload
        let headerData = try! JSONSerialization.data(withJSONObject: header, options: [])
        let payloadData = try! JSONSerialization.data(withJSONObject: payload, options: [])
        let encodedHeader = replacingOccurrences(data: headerData.base64EncodedString())
        let encodedPayload = replacingOccurrences(data: payloadData.base64EncodedString())

        let signingInput = "\(encodedHeader).\(encodedPayload)"
        let key = SymmetricKey(data: secretKey.data(using: .utf8)!)
        let signature = HMAC<SHA256>.authenticationCode(for: signingInput.data(using: .utf8)!, using: key)
        let encodedSignature = replacingOccurrences(data: Data(signature).base64EncodedString())
        let jwt = "\(encodedHeader).\(encodedPayload).\(encodedSignature)"
        return jwt
    }
    
    func replacingOccurrences(data:String) -> String {
        return data.replacingOccurrences(of: "+", with: "-").replacingOccurrences(of: "/", with: "_").replacingOccurrences(of: "=", with: "")
    }
}
