//
//  KeyChainHelper.swift
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2023/5/25.
//

import Foundation
import KeychainSwift

@objc public class KeyChainHelper: NSObject {
    
    @objc public func set(_ value: String?, forKey key: String) {
        if (key.isEmpty) {
            return
        }
        let keychain = KeychainSwift()
        keychain.set(value ?? "", forKey: key)
    }
    
    @objc public func get(_ key: String) -> String? {
        if (key.isEmpty) {
            return ""
        }
        let keychain = KeychainSwift()
        return keychain.get(key)
    }
    
    @objc public func setBool(_ value: Bool, forKey key: String) {
        if (key.isEmpty) {
            return
        }
        let keychain = KeychainSwift()
        keychain.set(value, forKey: key)
    }
    
    @objc public func getBool(_ key: String) -> Bool {
        if (key.isEmpty) {
            return false
        }
        let keychain = KeychainSwift()
        return keychain.getBool(key) ?? false
    }
    
    @objc public func delete(_ key: String) {
        if (key.isEmpty) {
            return
        }
        let keychain = KeychainSwift()
        keychain.delete(key)
    }
    
    @objc public func clear() {
        let keychain = KeychainSwift()
        keychain.clear()
    }
    
    @objc public func allkeys() -> Array<Any> {
        let keychain = KeychainSwift()
        return keychain.allKeys
    }
}
