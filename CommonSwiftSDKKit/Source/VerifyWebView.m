//
//  VerifyWebView.m
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2023/5/29.
//

#import "VerifyWebView.h"
#import <WebKit/WebKit.h>

@interface VerifyWebView()<WKNavigationDelegate,WKScriptMessageHandler>

@property (nonatomic, strong) WKWebView *webView;

@property (nonatomic, copy) VerifySuccess successBlock;
@property (nonatomic, copy) VerifyFail failBlock;


@end

@implementation VerifyWebView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    if (self = [super init]) {
        [self setUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setUI];
    }
    return self;
}

- (void)setUI
{
    [self addSubview:self.webView];
}

- (void)loadRequest:(NSString *)urlStr verifySuccess:(VerifySuccess)successBlock verifyFail:(VerifyFail)failBlock;
{
    if (urlStr.length > 0) {
        self.successBlock = successBlock;
        self.failBlock = failBlock;
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
        [self.webView loadRequest:urlRequest];
    }
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(nonnull WKUserContentController *)userContentController didReceiveScriptMessage:(nonnull WKScriptMessage *)message
{
    if (self.successBlock) {
        self.successBlock(message.body);
    }
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation
{
    NSLog(@"start Loading...");
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    NSLog(@"loading finish...");
}

- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if (self.failBlock) {
        self.failBlock(error);
    }
}

- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error
{
    if (self.failBlock) {
        self.failBlock(error);
    }
}

- (WKWebView *)webView {
    if (_webView == nil) {
        WKUserContentController *userController = [[WKUserContentController alloc] init];
        [userController addScriptMessageHandler:self name:@"JSBridge"];
        WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
        config.userContentController = userController;
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) configuration:config];
        _webView.navigationDelegate = self;
        _webView.backgroundColor = [UIColor whiteColor];
    }
    return _webView;
}

@end
