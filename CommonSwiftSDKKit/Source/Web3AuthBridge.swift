//
//  Web3AuthBridge.swift
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2023/5/12.
//

import Foundation
import Web3Auth
import secp256k1
import web3

@objc public class Web3AuthBridge: NSObject {
    
    @objc public func initWeb3Auth(clientId: String, network: String, sdkUrl: String, redirectUrl: String) async {
        let _ = await Web3Auth(W3AInitParams(
            clientId: clientId,
            network: transformNetwork(network: network),
            sdkUrl: URL(string: sdkUrl)!,
            redirectUrl: redirectUrl
        ))
    }

    @objc public func login(clientId: String, network:String, provider: String) async -> NSDictionary {
        do {
            let result = try await Web3Auth(.init(clientId: clientId, network: transformNetwork(network: network))).login(W3ALoginParams(loginProvider: transformProvider(provider: provider)))
            let userInfo:[String:String?] = ["aggregateVerifier": result.userInfo?.aggregateVerifier, "dappShare": result.userInfo?.dappShare, "email": result.userInfo?.email, "idToken": result.userInfo?.idToken, "name": result.userInfo?.name, "oAuthIdToken": result.userInfo?.oAuthIdToken, "oAuthAccessToken": result.userInfo?.oAuthAccessToken, "profileImage": result.userInfo?.profileImage, "typeOfLogin": result.userInfo?.typeOfLogin, "verifier": result.userInfo?.verifier, "verifierId": result.userInfo?.verifierId]
            let dict:[String:Any?] = ["publicKey": tranPublicKey(privkey: result.privKey ?? ""), "userInfo":userInfo, "address": transformAddress(user: result)]
            return dict as NSDictionary
        } catch {
            let dict:[String:Any?] = ["publicKey":"","userInfo":[:],"address":""]
            return dict as NSDictionary
        }
    }
    
    @objc public func logout() async{
        do {
            try await Web3Auth().logout()
        } catch {
            print("Logout Error")
        }
    }
    
    func tranPublicKey(privkey: String) -> String {
        if (privkey.isEmpty) {
            return ""
        }
        guard let privateKeyData = Data(hex: privkey) else {
            fatalError("Invalid private key")
        }
        // Convert the data to a byte array
        var privateKeyBytes = [UInt8](privateKeyData)
        // Create a new context for signing and verification
        let context = secp256k1_context_create(UInt32(SECP256K1_CONTEXT_SIGN|SECP256K1_CONTEXT_VERIFY))!
        // Create a new public key object
        var publicKey = secp256k1_pubkey()
        // Derive the public key from the private key
        let success = secp256k1_ec_pubkey_create(context, &publicKey, &privateKeyBytes)
        if success == 0 {
            fatalError("Error creating public key")
        }
        // Serialize the public key
        var serializedPublicKey = [UInt8](repeating: 0, count: 65)
        var outputLen = 65
        let serializedSuccess = secp256k1_ec_pubkey_serialize(context, &serializedPublicKey, &outputLen, &publicKey, UInt32(SECP256K1_EC_COMPRESSED))
        if serializedSuccess == 0 {
            fatalError("Error serializing public key")
        }
        // Encode the public key as a hexadecimal string
        let publicKeyHex = Data(serializedPublicKey[0..<outputLen]).toHexString()
        print("publicKeyHex = ", publicKeyHex)
        return publicKeyHex
    }
    
    func transformAddress(user: Web3AuthState) -> String {
        do {
            let account = try EthereumAccount(keyStorage: user )
            return account.address.value
        } catch {
            return ""
        }
    }
    
    
    func transformNetwork(network: String) -> Network {
        switch network {
        case "MAINNET":
            return .mainnet
        case "TESTNET":
            return .testnet
        case "CYAN":
            return .cyan
        default:
            return .testnet
        }
    }
    
    func transformProvider(provider: String) -> Web3AuthProvider? {
        switch provider {
        case "GOOGLE":
            return .GOOGLE
        case "REDDIT":
            return .REDDIT
        case "DISCORD":
            return .DISCORD
        case "TWITCH":
            return .TWITCH
        case "APPLE":
            return .APPLE
        case "TWITTER":
            return .TWITTER
        case "FACEBOOK":
            return .FACEBOOK
        case "LINE":
            return .LINE
        case "GITHUB":
            return .GITHUB
        case "KAKAO":
            return .KAKAO
        case "LINKEDIN":
            return .LINKEDIN
        case "WEIBO":
            return .WEIBO
        case "WECHAT":
            return .WECHAT
        case "EMAIL_PASSWORDLESS":
            return .EMAIL_PASSWORDLESS
        case "JWT":
            return .JWT
        default:
            return nil
        }
    }
}

extension Web3AuthState:EthereumKeyStorageProtocol {
    public func storePrivateKey(key: Data) throws {
        
    }
    
    public func loadPrivateKey() throws -> Data {
        guard let privKeyData = self.privKey?.web3.hexData else {
            throw SampleAppError.somethingWentWrong
        }
        return privKeyData
        
    }
}

public enum SampleAppError:Error{
    
    case noInternetConnection
    case decodingError
    case somethingWentWrong
    case customErr(String)
}

