//
//  ViewController.swift
//  CommonSwiftSDKKit
//
//  Created by yangpeng on 05/06/2023.
//  Copyright (c) 2023 yangpeng. All rights reserved.
//

import UIKit
import CommonSwiftSDKKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        Task {
            let fingerPrintJS = FingerprintJSbridge()
            let fingerPrint = await fingerPrintJS.generateTree()
            let devicedID = await fingerPrintJS.generateDevicedId()
            print("----2",fingerPrint, devicedID)
        }
        
        let loginButton = UIButton(frame: CGRectMake(20, 100, 80, 40))
        loginButton.setTitle("google", for: .normal)
        loginButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        loginButton.setTitleColor(UIColor.white, for: .normal)
        loginButton.backgroundColor = UIColor.purple
        loginButton.addTarget(self, action:#selector(loginClick), for: .touchUpInside)
        self.view.addSubview(loginButton)
        
        let keychainButton = UIButton(frame: CGRectMake(140, 100, 80, 40))
        keychainButton.setTitle("KeyChain", for: .normal)
        keychainButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        keychainButton.setTitleColor(UIColor.white, for: .normal)
        keychainButton.backgroundColor = UIColor.purple
        keychainButton.addTarget(self, action:#selector(keyChainClick), for: .touchUpInside)
        self.view.addSubview(keychainButton)
        
        let webButton = UIButton(frame: CGRectMake(260, 100, 80, 40))
        webButton.setTitle("web", for: .normal)
        webButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        webButton.setTitleColor(UIColor.white, for: .normal)
        webButton.backgroundColor = UIColor.purple
        webButton.addTarget(self, action:#selector(webClick), for: .touchUpInside)
        self.view.addSubview(webButton)
        
    }
    
    @objc func keyChainClick(){
        let keychain = KeyChainHelper()
        print("keychain1", keychain.get("jwt_token") ?? "")
        keychain.set("1234567890", forKey: "jwt_token")
        print("keychain2", keychain.get("jwt_token") ?? "")
        print("keychain3", keychain.allkeys())
        keychain.delete("jwt_token")
        print("keychain4", keychain.get("jwt_token") ?? "")
    }
    
    @objc func webClick(){
        
        let payload : [String: Any] = [
            "sub": "1234567890",
            "name": "John Doe",
            "iat": "1516239022"
        ]
        
        let jwt = JwtBridge().createJwt(secretKey: "0XtdLsYulHv0XCDB+Rm0l5dDt8le1xoONjFUSkX6GsA=", payload: payload);
        print("jwt = \(jwt)")
        //https://m.dipbit.xyz/captcha/google-v3.html
//        let webview = VerifyWebView(frame: CGRect(x: 0, y: 0, width: 400, height: 600))
//        webview.loadRequest("https://m.dipbit.xyz/captcha/index.html?language=hk", verifySuccess: { (messageBody) -> () in
//            print("123",messageBody)
//        }, verifyFail: { (error) -> () in
//            print("456",error.localizedDescription)
//        })
//        self.view .addSubview(webview)
    }
    
    
    @objc func loginClick(){
        //web3Auth 登录
        Task {
            //web3Auth 初始化
            await Web3AuthBridge().initWeb3Auth(clientId: "BCP0RdVtKYtjMkKht8KfOvuRVoi-Xs4Di-iKwO04gA2AoNB5dt0eCtduhkF-wQpZXb4_jTgCAz6FwgHRcaL1jrw", network: "testnet", sdkUrl: "https://", redirectUrl:"com.mugen.odyssey")

            let web3Dict = await Web3AuthBridge().login(clientId: "BCP0RdVtKYtjMkKht8KfOvuRVoi-Xs4Di-iKwO04gA2AoNB5dt0eCtduhkF-wQpZXb4_jTgCAz6FwgHRcaL1jrw", network: "testnet", provider: "GOOGLE")
            print(web3Dict)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

