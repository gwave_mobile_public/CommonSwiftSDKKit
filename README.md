# CommonSwiftSDKKit

[![CI Status](https://img.shields.io/travis/yangpeng/CommonSwiftSDKKit.svg?style=flat)](https://travis-ci.org/yangpeng/CommonSwiftSDKKit)
[![Version](https://img.shields.io/cocoapods/v/CommonSwiftSDKKit.svg?style=flat)](https://cocoapods.org/pods/CommonSwiftSDKKit)
[![License](https://img.shields.io/cocoapods/l/CommonSwiftSDKKit.svg?style=flat)](https://cocoapods.org/pods/CommonSwiftSDKKit)
[![Platform](https://img.shields.io/cocoapods/p/CommonSwiftSDKKit.svg?style=flat)](https://cocoapods.org/pods/CommonSwiftSDKKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CommonSwiftSDKKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CommonSwiftSDKKit'
```

## Author

yangpeng, peng.yang@kikitrade.com

## License

CommonSwiftSDKKit is available under the MIT license. See the LICENSE file for more info.
